FROM atlassian/pipelines-awscli:1.18.152

COPY pipe.sh /
RUN wget -P / https://bitbucket.org/bitbucketpipelines/bitbucket-pipes-toolkit-bash/raw/0.4.0/common.sh
RUN chmod a+x /*.sh

ENTRYPOINT ["/pipe.sh"]

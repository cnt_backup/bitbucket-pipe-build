#!/usr/bin/env bash
#
# This pipe is an example to show how easy is to create pipes for Bitbucket Pipelines.
#

source "$(dirname "$0")/common.sh"

info "Executing the pipe..."

# Required parameters
PROJECT=${PROJECT:?'PROJECT variable missing.'}
AWS_REGION=${AWS_REGION:?'AWS_REGION variable missing.'}
AWS_REPOSITORY_PREFIX=${AWS_REPOSITORY_PREFIX:?'AWS_REPOSITORY_PREFIX variable missing.'}
AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID:?'AWS_ACCESS_KEY_ID variable missing.'}
AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY:?'AWS_SECRET_ACCESS_KEY variable missing.'}

# Default parameters
EXTRA_BUILD_ARGS=${EXTRA_BUILD_ARGS:=""}
DEBUG=${DEBUG:="false"}

set -ex
declare -a DOCKER_TAGS
if [[ "$BITBUCKET_TAG" =~ ^[0-9]+(\.[0-9]+){2}$ ]]; then
  DOCKER_TAGS+=("$BITBUCKET_TAG")
  DOCKER_TAGS+=("stable")
fi
if [[ "$BITBUCKET_BRANCH" =~ ^(master|develop)$ ]]; then
  DOCKER_TAGS+=("$BITBUCKET_BRANCH")
fi
if [[ "$BITBUCKET_BRANCH" =~ ^feature\/(.+) ]]; then
  DOCKER_TAGS+=("feature-${BASH_REMATCH[1]}")
fi
if [[ "$BITBUCKET_BRANCH" =~ ^stage\/(.+)$ ]]; then
  DOCKER_TAGS+=("stage-${BASH_REMATCH[1]}")
fi
if [[ "$BITBUCKET_BRANCH" =~ ^(hotfix|release)\/(.+)$ ]]; then
  DOCKER_TAGS+=("rc-${BASH_REMATCH[2]}")
fi
if [[ -n "$DOCKER_TAGS" ]]; then
  set +x # Disable output of the aws login
  echo "y" | eval $(aws --region ${AWS_REGION} ecr get-login --no-include-email)
  set -x
  run bash -c "docker build -t \"${PROJECT}\" --build-arg BUILD_HASH=$(git rev-parse --short HEAD) --build-arg APP_VERSION='$([ -n "${BITBUCKET_TAG}" ] && echo "V${BITBUCKET_TAG}" || echo "version ${BITBUCKET_BRANCH}-${BITBUCKET_COMMIT:0:7}")' $(echo -n ${EXTRA_BUILD_ARGS}) ."
  for TAG in "${DOCKER_TAGS[@]}"; do
    docker tag "${PROJECT}" "${AWS_REPOSITORY_PREFIX}${PROJECT}:${TAG}"
    docker push "${AWS_REPOSITORY_PREFIX}${PROJECT}:${TAG}"
  done

  if [[ "${status}" == "0" ]]; then
    success "Success!"
  else
    fail "Error!"
  fi
else
  success "Success!"
fi
